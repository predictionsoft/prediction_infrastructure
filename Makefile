.PHONY: update_spa_principal update_gateway update_mc_auth update_mc_file_update update_infrastructure update_mc_machine_learning

HOME_DIR := $(HOME)

update_spa_principal:
		cd $(HOME_DIR)/projects/prediction_spa_principal && \
		git pull origin main && \
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		docker-compose build --no-cache spa-principal && \
		docker-compose up -d --force-recreate --no-deps spa-principal

update_gateway:
		cd $(HOME_DIR)/projects/prediction_api_gateway && \
		git pull origin main && \
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		docker-compose build --no-cache gateway && \
		docker-compose up -d --force-recreate --no-deps gateway

update_mc_auth:
		cd $(HOME_DIR)/projects/prediction_mc_auth && \
		git pull origin main && \
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		docker-compose build --no-cache mc-auth && \
		docker-compose up -d --force-recreate --no-deps mc-auth

update_mc_file_update:
		cd $(HOME_DIR)/projects/prediction_mc_file_update && \
		git pull origin main && \
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		docker-compose build --no-cache mc-file-update && \
		docker-compose up -d --force-recreate --no-deps mc-file-update

update_mc_machine_learning:
		cd $(HOME_DIR)/projects/prediction_mc_machine_learning && \
		git pull origin main && \
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		docker-compose build --no-cache mc-machine-learning && \
		docker-compose up -d --force-recreate --no-deps mc-machine-learning

update_infrastructure:
		cd $(HOME_DIR)/projects/prediction_infrastructure && \
		git pull origin main
